$MOD51	; This includes 8051 definitions for the Metalink assembler

; cell has 2 byte max value ff, min value 00
; a - ACC
; org 00h ; count nop

; start step for test RAM
start:
	mov P1, #00h ; initialized port 1 
	cpl P1.0 

	mov dptr, #800h ; start value adress zzz
	mov r3,#4 ; 4
	mov r2,#129 ; 129 ; 129*4=516byte
	mov r1,#0AAh ;xx

; test RAM 
test:
	mov a,r1 
	
	movx @dptr, a
	movx a, @dptr

	xrl a, r1 ; exclusive OR ; if a==r1 then a=0 else a=other_value

	jnz error_probe ; if a != 0 then error
	inc dptr ; next cell RAM
		
	djnz r2,test ; first cycle: r2-1
	mov r2, #129
	djnz r3,test ; two cycle: r3-1
	mov r2, #1
	cjne r3, #255, ok_probe
	

ok_probe:
	cpl P1.0 ; probe successful check RAM blink
	sjmp ok_probe

error_probe:
	cpl P1.0 ; probe successful check RAM = 0
	cpl P1.1 ; probe not successful check RAM = 1

error:
	sjmp error
	
END