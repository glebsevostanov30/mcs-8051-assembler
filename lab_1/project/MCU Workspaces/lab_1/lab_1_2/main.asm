$MOD51	; This includes 8051 definitions for the Metalink assembler

start:
	mov P1,#0h ; initiazlized P1
	mov R6,#0 ; initialized R6 as crc
	mov dptr,#800h ; start adress for check ROM (zzz - lab variable)
	mov R7,#4 ; count cycle

read_rom:
	clr A ; clear accamulator
	movc A, @A+dptr ; read byte ram from address A+dptr
	add A, R6 ; sum crc
	mov R6, A ; write crc
	inc dptr ; dptr + 1 - next byte

	djnz R7, read_rom ; if R7 != 0 then check_rom else continue

check_sum:
	clr A ; clear accamulator
	movc A, @A+dptr ; read value crc from rom

	xrl A, R6 ; exclusive OR ; if A==R6 then A=0 else A=other_value

	jnz error_probe ; if A != 0 then error

	cpl P1.0 ; on probe sucessfull
	sjmp $ ; while(1)

error_probe:
	cpl P1.1 ; on probe error
	sjmp error_probe

org 800h
db 1, 2, 3, 4, 10

END